# README #  
  
menu (wyświetla menu i zwraca nr wybranej opcji),  
obwodProstokata (liczy i zwraca obwód prostokąta, 2 parametry - liczby rzeczywiste),  
poleProstokata,  
obwodKwadratu (1 parametr, wykorzystuje funkcję obwodProstokata),  
poleKwadratu (1 parametr, wykorzystuje funkcję poleProstokata),  
poleSzescianu (pole powierzchni całkowitej, 1 parametr, wykorzystuje poleKwadratu),  
poleProstopadloscianu (3 parametry, wykorzystuje poleProstokata),  
zwiekszLitere ('a' => 'A' 'A' => 'A' '$' => '$')  
czyLitera ('a' => 1 'Z' => 1 '1' => 0 '%' => 0)   
