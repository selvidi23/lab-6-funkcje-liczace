#include <stdio.h>
#include <stdbool.h>
// menu (wyświetla menu i zwraca nr wybranej opcji),
// obwodProstokata (liczy i zwraca obwód prostokąta, 2 parametry - liczby rzeczywiste),
// poleProstokata,
// obwodKwadratu (1 parametr, wykorzystuje funkcję obwodProstokata),
// poleKwadratu (1 parametr, wykorzystuje funkcję poleProstokata),
// poleSzescianu (pole powierzchni całkowitej, 1 parametr, wykorzystuje poleKwadratu),
// poleProstopadloscianu (3 parametry, wykorzystuje poleProstokata),
// zwiekszLitere ('a' => 'A' 'A' => 'A' '$' => '$')
// czyLitera ('a' => 1 'Z' => 1 '1' => 0 '%' => 0) 

int menu(){
    int liczba;
    printf("\n");
    printf("Wybierz opcje \n");
    printf(" \n");

    printf("1. Obwod Prostokata \n");
    printf("2. Pole Prostokata \n");
    printf("3. Obwod Kwadratu \n");
    printf("4. Pole Kwadratu \n");
    printf("5. Pole Szescianu \n");
    printf("6. Pole Prostopdaloscianu \n");
    printf("7. Zwieksz Litere \n");
    printf("8. Sprawdz Czy To Litera \n");
    printf("0. Koniec Programu \n");
    printf(" \n");

    scanf("%d",&liczba);

    return liczba;
}

int circuit_Rectangle(int a,int b){
    return (2*a)+(2*b);
}

int rectangle_Area(int a,int b){
    return a*b;
}

int circuit_Square(int a){
    return circuit_Rectangle(a,a);
}

int square_Area(int a){
    return rectangle_Area(a,a);
}

int cube_Area(int a){
    return 6*square_Area(a);
}

int cuboid_Area(int a,int b, int c){
    return 2*rectangle_Area(a,b)+2*rectangle_Area(a,c)+2*rectangle_Area(b,c);
}

int increase_Letter(char a){
    if((int)a>97 && (int)a<122){
        return (int)a-32;
    }
    else{
        return (int)a;
    }
}

bool check_Letter(char a){

    if(a>='0' && a<='9'){
        return false;
    }else if (a >='A' && a<='Z' || a >= 'a' && a<='z')
    {
        return true;
    }
    else{
        return false;
    }


}




int main(){

        int a,b,c,select;
        char letter;
    do{
        select = menu();
        switch (select)
        {
        case 1:
            printf("Podaj liczbe a \n");
            scanf("%d",&a);
            printf("Podaj liczbe b \n");
            scanf("%d",&b);
            printf("Obwod Prostokata wynosi: %d \n",circuit_Rectangle(a,b));
            break;
        case 2:
            printf("Podaj liczbe a \n");
            scanf("%d",&a);
            printf("Podaj liczbe b \n");
            scanf("%d",&b);
            printf("Pole Prostokata wynosi: %d \n",rectangle_Area(a,b));
            break;
        case 3:
            printf("Podaj liczbe a \n");
            scanf("%d",&a);
            printf("Obwod Kwadratu wynosi: %d \n",circuit_Square(a));
            break;
        case 4:
            printf("Podaj liczbe a \n");
            scanf("%d",&a);
            printf("Pole Kwadratu wynosi: %d \n",square_Area(a));
            break;
        case 5:
            printf("Podaj liczbe a \n");
            scanf("%d",&a);
            printf("Pole Szescianu wynosi: %d \n",cube_Area(a));
            break;
        case 6:
            printf("Podaj liczbe a \n");
            scanf("%d",&a);
            printf("Podaj liczbe b \n");
            scanf("%d",&b);
            printf("Podaj Liczbe c\n");
            scanf("%d",&c);
            printf("Pole Prostopadloscianu: %d \n",cuboid_Area(a,b,c));
            break;
        case 7:
            printf("Podaj znak \n");
            scanf(" %c",&letter);
            printf("Znak po wykonaniu funkcji %c\n",increase_Letter(letter));
            break;
        case 8:
            printf("Podaj znak \n");
            scanf(" %c",&letter);
            if(check_Letter(letter)==true){
                printf("Wynik = 1");
            }else{
                printf("Wynik = 0");
            }
            break;
        }
    }while(select!=0); 
    return 0;
}
